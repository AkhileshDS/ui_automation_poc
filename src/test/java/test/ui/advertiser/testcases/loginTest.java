package test.ui.advertiser.testcases;

import java.net.MalformedURLException;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import test.ui.pages.advertiserPage;
import test.ui.pages.homePage;
import test.ui.pages.licenseePage;
import test.ui.pages.loginPage;
import test.ui.setup.testBase;

public class loginTest extends testBase {
	

		
		loginPage loginpage;
		licenseePage lp;
		advertiserPage ap;
		homePage hp;


		
		@BeforeMethod
		public void setUp() throws MalformedURLException{
			 initialization();
			 loginpage = new loginPage();
			 lp = new licenseePage();
			 ap = new advertiserPage();
			 hp = new homePage();
				RemoteWebDriver driver  = getDriver();

			 	driver.manage().window().maximize();
				driver.manage().deleteAllCookies();			
				driver.get(System.getProperty("url"));
			
		}
		
		@Test
		public void loginPageTitleTest() throws InterruptedException{
			loginpage.login("akhilesh.devarakonda@affle.com", "root");
			lp.selectLicensee("Myntra");
			hp.searchforAdvertiser("Test_advertiser_12_03");
			Assert.assertTrue(ap.verify_advertiser_name("Test_advertiser_12_03"));
			
		}
		
		
		@Test
		public void checkAdvertiser() throws InterruptedException{
			loginpage.login("akhilesh.devarakonda@affle.com", "root");
			lp.selectLicensee("Myntra");
			hp.searchforAdvertiser("Test_advertiser_12_03");
			Assert.assertTrue(ap.verify_advertiser_name("Test_advertiser_12_03"));
			
		}
		
		
		
		@AfterMethod
		public void teardown() {
			RemoteWebDriver driver  = getDriver();
			driver.quit();
		}
		

		
		
		

	


}
