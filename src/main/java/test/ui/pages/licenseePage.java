package test.ui.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import test.ui.setup.testBase;

public class licenseePage extends basepage{
	
	@FindBy(xpath = "//input[@ng-reflect-placeholder=\"Search\"]")
	WebElement search_field;
	
	//@FindBy
		
	
	public licenseePage() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	
	public void selectLicensee(String licenseeName) throws InterruptedException {
		Thread.sleep(9000);
		send_text(search_field,licenseeName);
		hit_enter(search_field);
		Thread.sleep(9000);

		click_element_with_text(licenseeName);
	}
}


//Test_advertiser_12_03