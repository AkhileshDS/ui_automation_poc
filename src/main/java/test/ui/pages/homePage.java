package test.ui.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class homePage extends basepage{
	
	@FindBy(id="menu-ADVERTISER")
	WebElement advertiser_button;
	
	@FindBy(xpath = "//a[@href=\"/advertiser/create\"]")
	WebElement advertiser_create_button;
	
	@FindBy(xpath = "//input[@placeholder=\"Search advertiser\"]")
	WebElement advertiser_search;
	
	public homePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void openCreateAdvertiserPage() {
		hover(advertiser_button);
		click_element(advertiser_create_button);
	}
	
	public void searchforAdvertiser(String advertiserName) throws InterruptedException {
		hover(advertiser_button);
		send_text(advertiser_search, advertiserName);
		click_element_with_text(advertiserName);
		
	}
	

}
