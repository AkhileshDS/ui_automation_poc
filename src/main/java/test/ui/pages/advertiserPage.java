package test.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import test.ui.setup.testBase;

public class advertiserPage extends basepage {
	
	@FindBy(xpath = "//div[@id='all-details']/app-adv-static-header/div/div[1]/div[1]")
	WebElement advertiser_title;
	
	public advertiserPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	
	public boolean verify_advertiser_name(String advertisername) {
		System.out.println(get_element_text(advertiser_title));
		String expression = "(//div[text() ='"+advertisername+"'])[1]";
		
		if (isElementdisplayed(advertiser_title)){
			return true;
		}
		else {
			return false;
		}
	}
	
	

}
