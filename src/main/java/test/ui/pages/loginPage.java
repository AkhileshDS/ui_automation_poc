package test.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import test.ui.setup.testBase;


public class loginPage extends basepage {
	
	
	@FindBy(id="user-name")
	WebElement username_field;
	
	@FindBy(id="password")
	WebElement password_field;
	
	@FindBy(id="login-btn")
	WebElement signin_button;
	
	
	
	public loginPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	public void login(String username, String password) throws InterruptedException {
		System.out.println("Inside Login method");
		send_text(username_field, username);
		send_text(password_field, password);
		click_element(signin_button);
		
	}
	

}
