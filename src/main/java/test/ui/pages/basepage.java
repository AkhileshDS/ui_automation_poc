package test.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import test.ui.setup.testBase;

public class basepage extends testBase  {
	
	
	public basepage() {
		super();
	}
	RemoteWebDriver driver  = getDriver();
	
	
	public void click_element(WebElement elem) {
		
		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		elem.click();
		 
	}
	
	public void send_text(WebElement elem, String text) throws InterruptedException {
		System.out.println(elem);
		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		elem.sendKeys(text);
		
	}
	
	public void hover(WebElement elem) {
		Actions actions = new Actions(driver);
		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		actions.moveToElement(elem).perform();
	}
	
	public void click_element_with_text(String text) {
		String expression = "//*[contains(text(),'"+text+"')]";
		System.out.println(expression);
		WebElement elem = driver.findElement(By.xpath(expression));
		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		elem.click();
		
	}
	
	public String get_element_text(WebElement elem) {
		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		System.out.println("In element text func");
		String s4 = elem.getText();
		System.out.println(s4);
		return s4;
		
	}
	
	public void hit_enter(WebElement elem) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		elem.sendKeys(Keys.ENTER);
		
	}
	
	public String get_url() {
		String s = driver.getCurrentUrl();
		return s;
	}
	
	public boolean isElementdisplayed(WebElement elem) {
		WebDriverWait wait = new WebDriverWait(driver,20);
		elem= wait.until(ExpectedConditions.visibilityOf(elem));
		return elem.isDisplayed();
		
	}
	
	
	
	
	
	

}
