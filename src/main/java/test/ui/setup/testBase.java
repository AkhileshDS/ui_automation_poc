
package test.ui.setup;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;


public class testBase {

		
	public static ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();
		
		public static void initialization() throws MalformedURLException{
			ChromeOptions chromeOptions = new ChromeOptions();
			DesiredCapabilities capabilities = new  DesiredCapabilities();
			capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		    driver.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities));		
			
		}
	    
	    public RemoteWebDriver getDriver() {
	    	return driver.get();
	    }
		
	    
	    

}
